/* Amplify Params - DO NOT EDIT
	ENV
	REGION
	STORAGE_CHALLENGE_ARN
	STORAGE_CHALLENGE_NAME
	STORAGE_CHALLENGE_STREAMARN
Amplify Params - DO NOT EDIT */

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */

 const AWS = require('aws-sdk');
 const docClient = new AWS.DynamoDB.DocumentClient();

async function listItems(state){
    const params = {
        TableName : process.env.STORAGE_CHALLENGE_NAME
    }

    if(state){
        params.FilterExpression= "#state = :state ";
        params.ExpressionAttributeNames = {
            "#state" : "state"
          };
    
          params.ExpressionAttributeValues = {
            ":state" : state
          };
    }
    
    const data = await docClient.scan(params).promise();
    data.Items.forEach(room => {
        var players = [];
        if(room.players){
            room.players.forEach(element => {
                players.push({
                    id: element
                });
            });
            room.players = players;
        }
    });
    return data;
};

exports.handler = async (event) => {
    console.info('Query listchallenges');
    
    const state = event.arguments.state;
    var list = await listItems(state);
    return list.Items;
};
