/* Amplify Params - DO NOT EDIT
	ENV
	REGION
	STORAGE_QUIZ_BUCKETNAME
Amplify Params - DO NOT EDIT */

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */

const AWS = require('aws-sdk');
const s3Client = new AWS.S3();


function getProfileUrl(userId, write){
    return new Promise((resolve,reject)=>{
        var params = {
            Bucket: process.env.STORAGE_QUIZ_BUCKETNAME,
            Key: "profiles/"+userId+".jpg"
        };

        if(write){
            params["ContentType"] = "image/jpeg";
            params["Expires"] = 60 * 60 * 60;
            s3Client.getSignedUrl("putObject", params, (error, url) => {
                if (error) {
                    console.log(error, error.stack);
                    reject(error)
                } else {
                    resolve(url);
                }
            });

        }else{
            
            s3Client.headObject(params, function(err) {
                
                if (err && err.code === 'NotFound') {  
                    params.Key = "profiles/default.png"
                }

                params["Expires"] = 60 * 60 * 60;
                s3Client.getSignedUrl("getObject", params, (error, url) => {
                    if (error) {
                        console.log(error, error.stack);
                        reject(error)
                    } else {
                        resolve(url);
                    }
                });
                
            });
        }
    });
}
  
  
  
exports.handler = async (event) => {
    console.log(`EVENT: ${JSON.stringify(event)}`);

    const player = event.source;
    return await getProfileUrl(player.id, (event.fieldName === "uploadUrl"));
};
