/* Amplify Params - DO NOT EDIT
	API_SHORTCUTSQUIZ_GRAPHQLAPIENDPOINTOUTPUT
	API_SHORTCUTSQUIZ_GRAPHQLAPIIDOUTPUT
	API_SHORTCUTSQUIZ_GRAPHQLAPIKEYOUTPUT
	ENV
	REGION
	STORAGE_CHALLENGE_ARN
	STORAGE_CHALLENGE_NAME
	STORAGE_CHALLENGE_STREAMARN
Amplify Params - DO NOT EDIT */

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */

const { v4 }  = require('uuid');
const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();
const fetch = require( 'node-fetch');


async function createItem(){
    var newData = {
        id : v4(), 
        state: "CREATED",
        players : []
    };
    var params = {
        TableName : process.env.STORAGE_CHALLENGE_NAME,
        Item: newData
    }

    await docClient.put(params).promise();
    return newData;
};

async function notifyChallengeCreated(roomId){

    console.info('notify create of challenge ' +roomId);
    const query = /* GraphQL */ `
        mutation notifyChallengeCreated($roomId: String!) {
            addEventToRoom(roomId: $roomId, eventType: CREATE) {
                id
                roomId
                type
            }
        }

    `;

    const variables = {
        roomId: roomId
    };

    const options = {
        method: 'POST',
        headers: {
            'x-api-key': process.env.API_SHORTCUTSQUIZ_GRAPHQLAPIKEYOUTPUT
        },
        body: JSON.stringify({ query, variables })
    };

    var response = await fetch(process.env.API_SHORTCUTSQUIZ_GRAPHQLAPIENDPOINTOUTPUT, options);
    var body = await response.json()
    console.debug(`("---------->"+: ${JSON.stringify(body)}`);


};

exports.handler = async (event) => {
    console.info('Mutation createChallenge');
    const item =  await createItem();
    await notifyChallengeCreated(item.id);
    return item;
};
