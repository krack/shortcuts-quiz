/* Amplify Params - DO NOT EDIT
	AUTH_SHORTCUTSQUIZ71DE7471_USERPOOLID
	ENV
	REGION
Amplify Params - DO NOT EDIT */// This is sample code. Please update this to suite your schema

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */


const { CognitoIdentityServiceProvider } = require('aws-sdk');
const cognitoIdentityServiceProvider = new CognitoIdentityServiceProvider();

async function resolvePlayer(player){
    var players = [];
    var params = {
        UserPoolId: process.env.AUTH_SHORTCUTSQUIZ71DE7471_USERPOOLID, 
        Filter: 'sub="'+player.id+'"',
    };
   var data = await cognitoIdentityServiceProvider.listUsers(params).promise();
    console.log(data);  
    data.Users.forEach(user =>{
        players.push({
            id: player.id,
            name: user.Username,
            profilePicture: {
                id: player.id
            }
        })
    });
    return players;
}

exports.handler = async (event) => {
    console.log(`EVENT: ${JSON.stringify(event)}`);

    const sourcePlayers = event.source.players;
    const sourcePlayer = event.source.player;
    if(sourcePlayers){
        var players = [];
        for(var i = 0; i < sourcePlayers.length; i++){
            var player = sourcePlayers[i];
            var tmpPlayer = await resolvePlayer(player);
            for(var j = 0; j < tmpPlayer.length; j++){
                players.push(tmpPlayer[j]);
            }
           
        }
        return players;
    }else if(sourcePlayer){

        var tmpPlayer = await resolvePlayer(sourcePlayer);
    
        if(tmpPlayer.length > 0){
            return tmpPlayer[0];
        }else{
            return null;
        }
    
    } else{
        return null;
    }
};

