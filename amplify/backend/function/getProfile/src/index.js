/* Amplify Params - DO NOT EDIT
	AUTH_SHORTCUTSQUIZ71DE7471_USERPOOLID
	ENV
	REGION
Amplify Params - DO NOT EDIT */

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */
 const { CognitoIdentityServiceProvider } = require('aws-sdk');
 const cognitoIdentityServiceProvider = new CognitoIdentityServiceProvider();
 
 async function resolvePlayer(id){
     var players = [];
     var params = {
         UserPoolId: process.env.AUTH_SHORTCUTSQUIZ71DE7471_USERPOOLID, 
         Filter: 'sub="'+id+'"',
     };
    var data = await cognitoIdentityServiceProvider.listUsers(params).promise();
     console.log(data);  
     data.Users.forEach(user =>{
         players.push({
             id: id,
             name: user.Username,
             profilePicture:{
                id: id
             }
         })
     });
     return players;
 }
 
 exports.handler = async (event) => {
     console.log(`EVENT: ${JSON.stringify(event)}`);
     const sub = event.identity.sub;
     var players =  await resolvePlayer(sub);
     if(players.length == 1){
        return players[0];
     }else{
        return null;
     }
 };
 
 