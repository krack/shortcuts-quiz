/* Amplify Params - DO NOT EDIT
	API_SHORTCUTSQUIZ_GRAPHQLAPIENDPOINTOUTPUT
	API_SHORTCUTSQUIZ_GRAPHQLAPIIDOUTPUT
	API_SHORTCUTSQUIZ_GRAPHQLAPIKEYOUTPUT
	ENV
	REGION
	STORAGE_CHALLENGE_ARN
	STORAGE_CHALLENGE_NAME
	STORAGE_CHALLENGE_STREAMARN
Amplify Params - DO NOT EDIT */

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */


const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();
const fetch = require( 'node-fetch');

function findIndexes(arr, value) {
   var index = [];
   for(var i = 0; i < arr.length; i++){
       if (arr[i] === value) {
           index.push(i);
       }

   }
   return index;
}



async function deletePlayerOfRoom(roomId, indexes){

    if(indexes.length > 0){

        console.log("indexes ", indexes);
        var expression = "REMOVE ";
        for(var i =0; i < indexes.length; i++ ){
            expression += ` players[${indexes[i]}],`
        }
        expression = expression.slice(0, -1);
        console.log("Expression "+expression);

        var params = {
            TableName : process.env.STORAGE_CHALLENGE_NAME,
            Key: {
                'id': roomId
            },
            UpdateExpression : expression
        }

        await docClient.update(params).promise();
    }
}

async function removePlayer(roomId, playerId){
   
    var params = {
        TableName : process.env.STORAGE_CHALLENGE_NAME,
        Key: {'id': roomId}
    }


    var challenge = await docClient.get(params).promise();
    
    var indexes = findIndexes(challenge.Item.players, playerId);
    deletePlayerOfRoom(roomId, indexes);
};

async function addPlayer(chanllengeId, playerId){
  
    removePlayer(chanllengeId, playerId);
    var players = [playerId];

    var params = {
        TableName : process.env.STORAGE_CHALLENGE_NAME,
         
        Key: {
          'id': chanllengeId
        },
        UpdateExpression : "SET #attrName = list_append(#attrName, :attrValue)",
        ExpressionAttributeNames : {
          "#attrName" : "players"
        },
        ExpressionAttributeValues : {
          ":attrValue" : players
        }
      }

    await docClient.update(params).promise();
};

async function notifyChallengeCreated(roomId, playerId){

    console.info('notify connection in challenge ' +roomId+" of player "+playerId);
    const query = /* GraphQL */ `
        mutation notifyChallengeCreated($roomId: String!, $playerId: String) {
            addEventToRoom(roomId: $roomId, eventType: CONNECT_PLAYER, playerId: $playerId) {
                id
                roomId
                type
                player{
                    id
                    name
                    profilePicture{
                        url
                        uploadUrl
                    }
                }
            }
        }

    `;

    const variables = {
        roomId: roomId,
        playerId: playerId
    };

    const options = {
        method: 'POST',
        headers: {
            'x-api-key': process.env.API_SHORTCUTSQUIZ_GRAPHQLAPIKEYOUTPUT
        },
        body: JSON.stringify({ query, variables })
    };

    var response = await fetch(process.env.API_SHORTCUTSQUIZ_GRAPHQLAPIENDPOINTOUTPUT, options);
    var body = await response.json()
    console.debug(`("---------->"+: ${JSON.stringify(body)}`);


};

exports.handler = async (event) => {
    console.info('Mutation connectChallenge');

    console.log(`EVENT: ${JSON.stringify(event)}`);
    const roomId = event.arguments.id;
    const sub = event.identity.sub;
    await addPlayer(roomId, sub);
    await notifyChallengeCreated(roomId, sub);
    return event.arguments.id;
};