/* Amplify Params - DO NOT EDIT
	ENV
	REGION
	STORAGE_CHALLENGE_ARN
	STORAGE_CHALLENGE_NAME
	STORAGE_CHALLENGE_STREAMARN
Amplify Params - DO NOT EDIT */

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */


 const AWS = require('aws-sdk');
 const docClient = new AWS.DynamoDB.DocumentClient();

async function getItem(id){
    const params = {
        TableName : process.env.STORAGE_CHALLENGE_NAME,
        Key: {'id': id}
    }
    
    const data = await docClient.get(params).promise();

    var players = [];
    if(data.Item){
        if(data.Item.players){
            data.Item.players.forEach(element => {
                players.push({
                    id: element
                });
            });
            data.Item.players = players;
        }

        cleanQuestionNotSend(data.Item);
        completeQuestions(data.Item);
    }


    return data.Item;
};

function cleanQuestionNotSend(room){
    if(room.questions){
        room.questions = room.questions.filter(question => question.send);
    }
}

function completeQuestions(room){
    if(room.questions){
        room.questions.forEach(question => {
            var seconds = 20;
            question.startAt = new Date(question.send);
            question.deadline = new Date(question.startAt.getTime() + (1000 * seconds));
        });
    }
}

exports.handler = async (event) => {
    console.info('Query getRoom');
    const roomId = event.arguments.id;
    var room = await getItem(roomId);
    return room;
};
