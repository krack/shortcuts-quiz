/* Amplify Params - DO NOT EDIT
	API_SHORTCUTSQUIZ_GRAPHQLAPIENDPOINTOUTPUT
	API_SHORTCUTSQUIZ_GRAPHQLAPIIDOUTPUT
	API_SHORTCUTSQUIZ_GRAPHQLAPIKEYOUTPUT
	ENV
	REGION
	STORAGE_CHALLENGE_ARN
	STORAGE_CHALLENGE_NAME
	STORAGE_CHALLENGE_STREAMARN
Amplify Params - DO NOT EDIT */

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */

 const AWS = require('aws-sdk');
 const docClient = new AWS.DynamoDB.DocumentClient();
 const fetch = require( 'node-fetch');
 const s3Client = new AWS.S3();
 

 async function selectQuestion(roomId){
     const paramsGet = {
         TableName : process.env.STORAGE_CHALLENGE_NAME,
         Key: {'id': roomId}
     }
     
     var room = await docClient.get(paramsGet).promise();
     
     var question = room.Item.questions.find(question => !question.send);
     if(question){
        question.send = Date.now();
        
        
        var paramsPut = {
            TableName : process.env.STORAGE_CHALLENGE_NAME,
            Item: room.Item
        }
    
        console.info(await docClient.put(paramsPut).promise());


        var seconds = 20;
        question.startAt = new Date(question.send);
        question.deadline = new Date(question.startAt.getTime() + (1000 * seconds));

        return question;
    }else{
        return null;
    }
 };

function computeScore(room){
    var scores = [];
    // for each player connected : add score : 0
    room.players.forEach(player => {

        scores.push({
            player:{
                id: player
            } ,
            score: 0
        });
    });

    // for each question
    room.questions.forEach(question => {
        if(question.results){
            question.results.forEach(result => {

                var score = scores.find(score => score.player.id === result.playerId);
                // if player disconnected
                if(!score){
                    score = {
                        player:{
                            id: result.playerId
                        } ,
                        score: 0
                    }
                    scores.push(score);
                }
                
                score.score = score.score + ((question.send + (20*1000)) - result.date);

            });
        }
    });

    return scores;
}

 async function closeRoom(roomId){
    const paramsGet = {
        TableName : process.env.STORAGE_CHALLENGE_NAME,
        Key: {'id': roomId}
    }
    
    var room = await docClient.get(paramsGet).promise();
    //compute score 
    var scores = computeScore(room.Item);

      
    scores.sort( ( a, b ) =>{
    if ( a.score < b.score ){
        return 1;
    }
    if ( a.score > b.score ){
        return -1;
    }
    return 0;
    } );

    for(var i = 0; i < scores.length; i++){
        scores[i].rank= (i+1);
    }
    console.log(scores);
    await sendScore(roomId, scores);
    await deleteRoom(roomId);

   
};

async function deleteRoom(roomId){

    console.info('delete room after end ' +roomId);
    var params = {
        TableName : process.env.STORAGE_CHALLENGE_NAME,
        Key: {
        'id': roomId
        }
    }

    await docClient.delete(params).promise();
}
  

async function sendScore(roomId, scores){
  
    console.info('notify end of room' +roomId);
    const query = /* GraphQL */ `
        mutation notifyChallengeCreated($roomId: String!, $result: ResultInput) {
            addEventToRoom(roomId: $roomId, eventType: END, result: $result) {
               id
               roomId
               type
               result {
                    scores{
                        player{
                            id
                            name
                            profilePicture{
                                url
                                uploadUrl
                            }
                        }
                        score
                        rank
                    }
               }
            }
        }

    `;
    const variables = {
        roomId: roomId,
        result: {
            scores: scores
        }
    };

    const options = {
        method: 'POST',
        headers: {
            'x-api-key': process.env.API_SHORTCUTSQUIZ_GRAPHQLAPIKEYOUTPUT
        },
        body: JSON.stringify({ query, variables })
    };

    var response = await fetch(process.env.API_SHORTCUTSQUIZ_GRAPHQLAPIENDPOINTOUTPUT, options);

    var body = await response.json()
    console.debug(`("---------->"+: ${JSON.stringify(body)}`);


};

  async function notifyNewQuestion(roomId, question){
  
      console.info('notify new question for ' +roomId+' with question '+question.id +' '+question.description);
      const query = /* GraphQL */ `
          mutation notifyChallengeCreated($roomId: String!, $question: QuestionInput) {
            addEventToRoom(roomId: $roomId, eventType: NEW_QUESTION, question: $question) {
                 id
                 roomId
                 type
                 question {
                    id
                    description
                    startAt
                    deadline
                 }
              }
          }
  
      `;
      const variables = {
          roomId: roomId,
          question: {
            id: question.id,
            description: question.description,
            startAt: question.startAt,
            deadline: question.deadline
          }
      };
  
      const options = {
          method: 'POST',
          headers: {
              'x-api-key': process.env.API_SHORTCUTSQUIZ_GRAPHQLAPIKEYOUTPUT
          },
          body: JSON.stringify({ query, variables })
      };
  
      var response = await fetch(process.env.API_SHORTCUTSQUIZ_GRAPHQLAPIENDPOINTOUTPUT, options);
 
      var body = await response.json()
      console.debug(`("---------->"+: ${JSON.stringify(body)}`);
  
  
  };
  
 exports.handler = async (event) => {
     console.log(`EVENT: ${JSON.stringify(event)}`);
     const roomId = event.arguments.roomId;
     var question = await selectQuestion(roomId);
     if(question){
        await notifyNewQuestion(roomId, question);
     }else{
        await closeRoom(roomId);

     }
     return roomId;
 };