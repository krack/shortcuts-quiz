
/* Amplify Params - DO NOT EDIT
	API_SHORTCUTSQUIZ_GRAPHQLAPIENDPOINTOUTPUT
	API_SHORTCUTSQUIZ_GRAPHQLAPIIDOUTPUT
	API_SHORTCUTSQUIZ_GRAPHQLAPIKEYOUTPUT
	ENV
	REGION
	STORAGE_CHALLENGE_ARN
	STORAGE_CHALLENGE_NAME
	STORAGE_CHALLENGE_STREAMARN
	STORAGE_QUIZ_BUCKETNAME
Amplify Params - DO NOT EDIT */

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */



const AWS = require('aws-sdk');
const docClient = new AWS.DynamoDB.DocumentClient();
const fetch = require( 'node-fetch');
const s3Client = new AWS.S3();


async function loadQuestions(){
    var params = {
        Bucket: process.env.STORAGE_QUIZ_BUCKETNAME,
        Key: "questions.json"
    };

    var json_data = await s3Client.getObject(params).promise();
    var questions = JSON.parse(json_data.Body.toString());
    return questions;
}

function selectionQuestions(questions, numberOfQuestions){
    var selectedQuestions =  [];
    for(var i = 0; i < numberOfQuestions; i++){
        var index = Math.floor(Math.random() * questions.length);
        selectedQuestions.push(questions[index]);
        questions.splice(index, 1);

    }
    return selectedQuestions;

}
 
async function updateRoomToStart(roomId){
    const paramsGet = {
        TableName : process.env.STORAGE_CHALLENGE_NAME,
        Key: {'id': roomId}
    }
    
    var room = await docClient.get(paramsGet).promise();
    room.Item.state = "STARTED";

    var questions = await loadQuestions();
    room.Item.questions = selectionQuestions(questions, 5);
    
    var paramsPut = {
        TableName : process.env.STORAGE_CHALLENGE_NAME,
        Item: room.Item
    }

    await docClient.put(paramsPut).promise();
};
 
 async function notifyRoomStart(roomId){
 
     console.info('notify room start ' +roomId);
     const query = /* GraphQL */ `
         mutation notifyChallengeCreated($roomId: String!) {
            addEventToRoom(roomId: $roomId, eventType: START) {
                id
                roomId
                type
             }
         }
 
     `;
     const variables = {
         roomId: roomId
     };
 
     const options = {
         method: 'POST',
         headers: {
             'x-api-key': process.env.API_SHORTCUTSQUIZ_GRAPHQLAPIKEYOUTPUT
         },
         body: JSON.stringify({ query, variables })
     };
 
     var response = await fetch(process.env.API_SHORTCUTSQUIZ_GRAPHQLAPIENDPOINTOUTPUT, options);

     var body = await response.json()
     console.debug(`("---------->"+: ${JSON.stringify(body)}`);
 
 
 };
 
exports.handler = async (event) => {
    console.log(`EVENT: ${JSON.stringify(event)}`);
    const roomId = event.arguments.id;
    await updateRoomToStart(roomId)
    await notifyRoomStart(roomId);
    return roomId;
};
