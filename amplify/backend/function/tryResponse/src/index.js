/* Amplify Params - DO NOT EDIT
	ENV
	REGION
	STORAGE_CHALLENGE_ARN
	STORAGE_CHALLENGE_NAME
	STORAGE_CHALLENGE_STREAMARN
	STORAGE_QUIZ_BUCKETNAME
Amplify Params - DO NOT EDIT */

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */


 const AWS = require('aws-sdk');
 const docClient = new AWS.DynamoDB.DocumentClient();
 const s3Client = new AWS.S3();

 async function checkReponse(questionId, reponse){
    var params = {
        Bucket: process.env.STORAGE_QUIZ_BUCKETNAME,
        Key: "questions.json"
    };

    
    var reponseToLower = reponse.map(e =>e.toLowerCase());

    
    var json_data = await s3Client.getObject(params).promise();
    var questions = JSON.parse(json_data.Body.toString());

    var question = questions.find(question => question.id === questionId);

    if (question.shortcut.length == reponseToLower.length
        && question.shortcut.every(function(u, i) {
            return reponseToLower.includes(u.toLowerCase());
        })
    ) {
       return true;
    } else {
        return false;
    }

}

async function registerSucess(roomId, questionId, playerId){
    const paramsGet = {
        TableName : process.env.STORAGE_CHALLENGE_NAME,
        Key: {'id': roomId}
    }
    
    var room = await docClient.get(paramsGet).promise();

    var question = room.Item.questions.find(question => question.id === questionId);
    if(!question.results){
        question.results = [];
    }
    question.results.push({
        playerId: playerId,
        date: Date.now()
    });
    
    var paramsPut = {
        TableName : process.env.STORAGE_CHALLENGE_NAME,
        Item: room.Item
    }

    await docClient.put(paramsPut).promise();

}

exports.handler = async (event) => {
    console.log(`EVENT: ${JSON.stringify(event)}`);

    var isValid = await checkReponse(event.arguments.questionId, event.arguments.essay);
    if(isValid){
        await registerSucess(event.arguments.roomId, event.arguments.questionId, event.identity.sub);
    }

    return isValid;
};
