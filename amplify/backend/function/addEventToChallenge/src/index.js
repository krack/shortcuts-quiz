/* Amplify Params - DO NOT EDIT
	ENV
	REGION
Amplify Params - DO NOT EDIT */

/**
 * @type {import('@types/aws-lambda').APIGatewayProxyHandler}
 */

const { v4 }  = require('uuid');
exports.handler = async (event) => {
    console.log(`EVENT: ${JSON.stringify(event)}`);
    const uuid = v4();
    console.info("addEventToRoom for room "+event.arguments.roomId+" => new event "+uuid+" of type "+event.arguments.eventType);
    var eventReturned = {
        id: uuid,
        roomId: event.arguments.roomId,
        type: event.arguments.eventType
    };

    if(event.arguments.playerId){
        eventReturned.player= {
            id: event.arguments.playerId
        };
    }
    if(event.arguments.question){
        eventReturned.question = event.arguments.question;
    }
    if(event.arguments.result){
        eventReturned.result = event.arguments.result;
    }

    return eventReturned;
};
