/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.
import { Injectable } from "@angular/core";
import API, { graphqlOperation, GraphQLResult } from "@aws-amplify/api-graphql";
import { Observable } from "zen-observable-ts";

export interface SubscriptionResponse<T> {
  value: GraphQLResult<T>;
}

export type __SubscriptionContainer = {
  subscribeToEventRoom: SubscribeToEventRoomSubscription;
};

export type Room = {
  __typename: "Room";
  id: string;
  state: StateType;
  players: Array<Player>;
  questions?: Array<Question> | null;
};

export enum StateType {
  CREATED = "CREATED",
  STARTED = "STARTED"
}

export type Player = {
  __typename: "Player";
  id: string;
  name: string;
  profilePicture: Picture;
};

export type Picture = {
  __typename: "Picture";
  url: string;
  uploadUrl: string;
};

export type Question = {
  __typename: "Question";
  id: string;
  description: string;
  startAt: string;
  deadline: string;
};

export enum EventType {
  CREATE = "CREATE",
  DELETE = "DELETE",
  CONNECT_PLAYER = "CONNECT_PLAYER",
  DISCONNECT_PLAYER = "DISCONNECT_PLAYER",
  START = "START",
  NEW_QUESTION = "NEW_QUESTION",
  END = "END"
}

export type QuestionInput = {
  id: string;
  description: string;
  startAt: string;
  deadline: string;
};

export type ResultInput = {
  scores: Array<PlayerScoreInput>;
};

export type PlayerScoreInput = {
  player: PlayerInput;
  score: number;
  rank: number;
};

export type PlayerInput = {
  id: string;
};

export type RoomEvent = {
  __typename: "RoomEvent";
  id: string;
  roomId: string;
  type: EventType;
  player?: Player | null;
  question?: Question | null;
  result?: Result | null;
};

export type Result = {
  __typename: "Result";
  scores: Array<PlayerScore>;
};

export type PlayerScore = {
  __typename: "PlayerScore";
  player?: Player | null;
  score: number;
  rank: number;
};

export type CreateRoomMutation = {
  __typename: "Room";
  id: string;
  state: StateType;
  players: Array<{
    __typename: "Player";
    id: string;
    name: string;
    profilePicture: {
      __typename: "Picture";
      url: string;
      uploadUrl: string;
    };
  }>;
  questions?: Array<{
    __typename: "Question";
    id: string;
    description: string;
    startAt: string;
    deadline: string;
  }> | null;
};

export type AddEventToRoomMutation = {
  __typename: "RoomEvent";
  id: string;
  roomId: string;
  type: EventType;
  player?: {
    __typename: "Player";
    id: string;
    name: string;
    profilePicture: {
      __typename: "Picture";
      url: string;
      uploadUrl: string;
    };
  } | null;
  question?: {
    __typename: "Question";
    id: string;
    description: string;
    startAt: string;
    deadline: string;
  } | null;
  result?: {
    __typename: "Result";
    scores: Array<{
      __typename: "PlayerScore";
      player?: {
        __typename: "Player";
        id: string;
        name: string;
        profilePicture: {
          __typename: "Picture";
          url: string;
          uploadUrl: string;
        };
      } | null;
      score: number;
      rank: number;
    }>;
  } | null;
};

export type ListRoomsQuery = {
  __typename: "Room";
  id: string;
  state: StateType;
  players: Array<{
    __typename: "Player";
    id: string;
    name: string;
    profilePicture: {
      __typename: "Picture";
      url: string;
      uploadUrl: string;
    };
  }>;
  questions?: Array<{
    __typename: "Question";
    id: string;
    description: string;
    startAt: string;
    deadline: string;
  }> | null;
};

export type GetRoomQuery = {
  __typename: "Room";
  id: string;
  state: StateType;
  players: Array<{
    __typename: "Player";
    id: string;
    name: string;
    profilePicture: {
      __typename: "Picture";
      url: string;
      uploadUrl: string;
    };
  }>;
  questions?: Array<{
    __typename: "Question";
    id: string;
    description: string;
    startAt: string;
    deadline: string;
  }> | null;
};

export type ProfileQuery = {
  __typename: "Player";
  id: string;
  name: string;
  profilePicture: {
    __typename: "Picture";
    url: string;
    uploadUrl: string;
  };
};

export type SubscribeToEventRoomSubscription = {
  __typename: "RoomEvent";
  id: string;
  roomId: string;
  type: EventType;
  player?: {
    __typename: "Player";
    id: string;
    name: string;
    profilePicture: {
      __typename: "Picture";
      url: string;
      uploadUrl: string;
    };
  } | null;
  question?: {
    __typename: "Question";
    id: string;
    description: string;
    startAt: string;
    deadline: string;
  } | null;
  result?: {
    __typename: "Result";
    scores: Array<{
      __typename: "PlayerScore";
      player?: {
        __typename: "Player";
        id: string;
        name: string;
        profilePicture: {
          __typename: "Picture";
          url: string;
          uploadUrl: string;
        };
      } | null;
      score: number;
      rank: number;
    }>;
  } | null;
};

@Injectable({
  providedIn: "root"
})
export class APIService {
  async CreateRoom(): Promise<CreateRoomMutation> {
    const statement = `mutation CreateRoom {
        createRoom {
          __typename
          id
          state
          players {
            __typename
            id
            name
            profilePicture {
              __typename
              url
              uploadUrl
            }
          }
          questions {
            __typename
            id
            description
            startAt
            deadline
          }
        }
      }`;
    const response = (await API.graphql(graphqlOperation(statement))) as any;
    return <CreateRoomMutation>response.data.createRoom;
  }
  async ConnectRoom(id: string): Promise<string | null> {
    const statement = `mutation ConnectRoom($id: ID!) {
        connectRoom(id: $id)
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <string | null>response.data.connectRoom;
  }
  async DisconnectRoom(id: string): Promise<string | null> {
    const statement = `mutation DisconnectRoom($id: ID!) {
        disconnectRoom(id: $id)
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <string | null>response.data.disconnectRoom;
  }
  async StartRoom(id: string): Promise<string | null> {
    const statement = `mutation StartRoom($id: ID!) {
        startRoom(id: $id)
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <string | null>response.data.startRoom;
  }
  async NextQuestionForRoom(roomId: string): Promise<string | null> {
    const statement = `mutation NextQuestionForRoom($roomId: ID!) {
        nextQuestionForRoom(roomId: $roomId)
      }`;
    const gqlAPIServiceArguments: any = {
      roomId
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <string | null>response.data.nextQuestionForRoom;
  }
  async TryResponse(
    roomId: string,
    questionId: string,
    essay: Array<string>
  ): Promise<boolean | null> {
    const statement = `mutation TryResponse($roomId: ID!, $questionId: ID!, $essay: [String!]!) {
        tryResponse(roomId: $roomId, questionId: $questionId, essay: $essay)
      }`;
    const gqlAPIServiceArguments: any = {
      roomId,
      questionId,
      essay
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <boolean | null>response.data.tryResponse;
  }
  async AddEventToRoom(
    roomId: string,
    eventType: EventType,
    playerId?: string,
    question?: QuestionInput,
    result?: ResultInput
  ): Promise<AddEventToRoomMutation> {
    const statement = `mutation AddEventToRoom($roomId: String!, $eventType: EventType!, $playerId: String, $question: QuestionInput, $result: ResultInput) {
        addEventToRoom(roomId: $roomId, eventType: $eventType, playerId: $playerId, question: $question, result: $result) {
          __typename
          id
          roomId
          type
          player {
            __typename
            id
            name
            profilePicture {
              __typename
              url
              uploadUrl
            }
          }
          question {
            __typename
            id
            description
            startAt
            deadline
          }
          result {
            __typename
            scores {
              __typename
              player {
                __typename
                id
                name
                profilePicture {
                  __typename
                  url
                  uploadUrl
                }
              }
              score
              rank
            }
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      roomId,
      eventType
    };
    if (playerId) {
      gqlAPIServiceArguments.playerId = playerId;
    }
    if (question) {
      gqlAPIServiceArguments.question = question;
    }
    if (result) {
      gqlAPIServiceArguments.result = result;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <AddEventToRoomMutation>response.data.addEventToRoom;
  }
  async ListRooms(state?: StateType): Promise<Array<ListRoomsQuery>> {
    const statement = `query ListRooms($state: StateType) {
        listRooms(state: $state) {
          __typename
          id
          state
          players {
            __typename
            id
            name
            profilePicture {
              __typename
              url
              uploadUrl
            }
          }
          questions {
            __typename
            id
            description
            startAt
            deadline
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (state) {
      gqlAPIServiceArguments.state = state;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <Array<ListRoomsQuery>>response.data.listRooms;
  }
  async GetRoom(id: string): Promise<GetRoomQuery> {
    const statement = `query GetRoom($id: ID!) {
        getRoom(id: $id) {
          __typename
          id
          state
          players {
            __typename
            id
            name
            profilePicture {
              __typename
              url
              uploadUrl
            }
          }
          questions {
            __typename
            id
            description
            startAt
            deadline
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetRoomQuery>response.data.getRoom;
  }
  async Profile(): Promise<ProfileQuery> {
    const statement = `query Profile {
        profile {
          __typename
          id
          name
          profilePicture {
            __typename
            url
            uploadUrl
          }
        }
      }`;
    const response = (await API.graphql(graphqlOperation(statement))) as any;
    return <ProfileQuery>response.data.profile;
  }
  SubscribeToEventRoomListener(
    roomId?: string
  ): Observable<
    SubscriptionResponse<Pick<__SubscriptionContainer, "subscribeToEventRoom">>
  > {
    const statement = `subscription SubscribeToEventRoom($roomId: String) {
        subscribeToEventRoom(roomId: $roomId) {
          __typename
          id
          roomId
          type
          player {
            __typename
            id
            name
            profilePicture {
              __typename
              url
              uploadUrl
            }
          }
          question {
            __typename
            id
            description
            startAt
            deadline
          }
          result {
            __typename
            scores {
              __typename
              player {
                __typename
                id
                name
                profilePicture {
                  __typename
                  url
                  uploadUrl
                }
              }
              score
              rank
            }
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (roomId) {
      gqlAPIServiceArguments.roomId = roomId;
    }
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<
      SubscriptionResponse<
        Pick<__SubscriptionContainer, "subscribeToEventRoom">
      >
    >;
  }
}
