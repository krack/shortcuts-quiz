import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Auth, Hub } from 'aws-amplify';
import { AuthenticatorService } from '@aws-amplify/ui-angular';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public user: any = null;

  constructor(private router: Router, public authenticator: AuthenticatorService) { }


  ngOnInit(): void {
  

  }

  disconnect(){
    this.authenticator.signOut();
    this.router.navigate(['/'])
  }


}
