import { Component, OnInit } from '@angular/core';
import { APIService, Player } from 'src/app/API.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public player : Player | null = null;

 private selectedFile: File | null = null;

  constructor(
    private roomsService: APIService,
    private http: HttpClient
    ) { }

  ngOnInit(): void {
    this.loadProfile();
  }

  private loadProfile(){
    this.roomsService.Profile().then(e => {
      this.player = e;
    });
  }

  
  updateProfile(): void{
    if(this.player){
      const headers = new HttpHeaders();
      headers.append('contentType', 'image/jpeg');
      const option = {headers: headers };
      const upload = this.http.put( this.player.profilePicture.uploadUrl, this.selectedFile, option).toPromise();
      upload
      .then(data => {
        this.loadProfile();
      })
      .catch(err => console.log('error: ', err)
      );
    }
  };

  onFileSelected(event: any): void {
    this.selectedFile = event.target.files[0] as File;
    this.updateProfile();
  };

  
}
