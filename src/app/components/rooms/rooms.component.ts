import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { APIService, EventType, Room, RoomEvent, StateType } from 'src/app/API.service';
import { CustomAPIService } from 'src/app/CustomAPI.service';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit, OnDestroy {

  public rooms: Room[] = [];
  private subscription: Subscription | null = null;
  public loading : boolean = false;
  public creating: boolean = false;

  constructor(
    private roomsService: APIService,
    private customAPIService: CustomAPIService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.registerEvents();
    this.listExistingRooms();
  }

  private registerEvents(): void {
    this.subscription = <Subscription>(
      this.customAPIService.SubscribeToEventChallengeListener().subscribe((event: any) => {
        const roomsEvent = event.value.data.subscribeToEventRoom as RoomEvent;
        if (roomsEvent.type == EventType.CREATE) {
          this.addRoom(roomsEvent);
        } else if (roomsEvent.type == EventType.CONNECT_PLAYER) {
          this.addPlayer(roomsEvent);
        } else if (roomsEvent.type == EventType.DISCONNECT_PLAYER) {
          this.removePlayer(roomsEvent);
        } else if (roomsEvent.type == EventType.DELETE) {
          this.deleteRoom(roomsEvent);
        } else if (roomsEvent.type == EventType.START) {
          this.deleteRoom(roomsEvent);
        }
      })
    );
  }

  private listExistingRooms(): void {
    this.loading = true;
    this.roomsService.ListRooms(StateType.CREATED).then(e => {
      this.rooms = e as Room[];
      this.loading = false;
    });
  }

  private addRoom(event: RoomEvent): void {
    console.log("create room with id " + event.roomId);
    const newChallenge = {
      id: event.roomId
    } as Room;
    this.rooms = [newChallenge, ...this.rooms];
  }

  private deleteRoom(event: RoomEvent): void {
    console.log("delete room with id " + event.roomId);
    this.rooms = this.rooms.filter(room => {
      return room.id !== event.roomId;
    });

  }

  private addPlayer(event: RoomEvent): void {
    console.log("Add player " + event.player?.id + " on room with id " + event.roomId);
    this.rooms.filter(room => room.id == event.roomId).forEach(room => {
      if (event.player) {
        if (!room.players) {
          room.players = [];
          room.players?.push(event.player);
        } else if (room.players?.filter(player => player?.id == event.player?.id).length == 0) {
          room.players?.push(event.player);
        }
      }
    });
  }

  private removePlayer(event: RoomEvent): void {
    console.log("Remove player " + event.player?.id + " on room with id " + event.roomId);
    this.rooms.filter(room => room.id == event.roomId).forEach(room => {
      if (event.player) {
        room.players = room.players?.filter(player => player?.id !== event.player?.id);
      }
    });
  }


  createRoom(): void {
    if(!this.creating){
      this.creating = true;
      this.roomsService.CreateRoom().then(e => {
        this.creating = false;
        this.router.navigate(['/room/' + e.id]);
      });
    }
  }

  connectToRoom(roomId: string): void {

    this.router.navigate(['/room/' + roomId]);
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }
}
