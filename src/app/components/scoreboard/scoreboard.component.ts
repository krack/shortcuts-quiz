import { Component, Input, OnInit } from '@angular/core';

import { Result } from 'src/app/API.service';

@Component({
  selector: 'app-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.scss']
})
export class ScoreboardComponent implements OnInit {
  @Input()
  public result!: Result;
  constructor() { }

  ngOnInit(): void {
  }

}
