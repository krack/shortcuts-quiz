import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { API, graphqlOperation } from 'aws-amplify';
import { Subscription } from 'rxjs';
import { APIService, EventType, Question, Result, Room, RoomEvent, StateType } from '../../API.service';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit, OnDestroy {
  private static ignoreKey = ["Control", "Alt", "Shift"];
  private subscription: Subscription | null = null;
  public room: Room | null = null;
  public question: Question | null = null;
  public started: boolean = false;
  public master: boolean = false;
  public result: Result|null = null;

  public responseSending : boolean = false;
  public currentShortcut : String[] = [];
  public currentFound : boolean = false;
  public startAt : Date | null = null;
  public deadline: Date | null = null;

  public starting : boolean= false;

  constructor(

    private roomService: APIService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.registerEvents(id);
    this.loadCurrentRoom(id);
  }

  private registerEvents(id: string|null): void {
    if (id) {
      this.subscription = <Subscription>(
        this.roomService.SubscribeToEventRoomListener(id).subscribe((event: any) => {
          const roomEvent = event.value.data.subscribeToEventRoom as RoomEvent;
          if (roomEvent.type == EventType.CONNECT_PLAYER) {
            this.addPlayer(roomEvent)

          } else if (roomEvent.type == EventType.DISCONNECT_PLAYER) {
            this.removePlayer(roomEvent);
          } else if (roomEvent.type == EventType.START) {
            this.startRoom(roomEvent);

          } else if (roomEvent.type == EventType.NEW_QUESTION) {
            this.loadQuestion(roomEvent);
          } else if (roomEvent.type == EventType.END) {
            this.loadResult(roomEvent);
          }

        })
      );
    }
  }

  private loadCurrentRoom(id: string|null): void {
    if (id) {
      this.roomService.GetRoom(id).then(room => {
        if(!room){
          this.router.navigate(['/rooms/']);
        }
        this.room = room;
        if(this.room.state == StateType.STARTED){
          this.started = true;
          if(this.room.players.length == 0){
            this.master = true;
          }
          if(this.room.questions){
            this.question = this.room.questions?.reverse()[0];
            this.startAt = new Date(this.question.startAt);
            this.deadline = new Date(this.question.deadline);
            if(this.master &&  (new Date > this.deadline)){
              this.nextQuestion();
            }
          }
        }
        this.connect();
      });
    }
  }

  private addPlayer(event: RoomEvent): void {
    if (event.player) {
      console.log("Add player of this room : " + event.player.id);
      if (this.room?.players?.filter(player => player?.id == event.player?.id).length == 0) {
        this.room.players?.push(event.player);
      }
    }
  }

  private removePlayer(event: RoomEvent): void {
    if (this.room) {
      if (event.player) {
        console.log("Delete player of this room : " + event.player.id);
        this.room.players = this.room?.players?.filter(player => player?.id !== event.player?.id);
      }
    }
  }

  private startRoom(event: RoomEvent): void {
    if (this.room) {
      console.log("The room start ");

      this.started = true;
      this.nextQuestion();
    }
  }
  private loadQuestion(event: RoomEvent): void {
    if (event.question) {

      this.resetCurrent();
      console.log("Load new question "+event.question.description);
      this.question = event.question;
      this.startAt = new Date(this.question.startAt);
      this.deadline = new Date(this.question.deadline);
      setTimeout(()=> {
        this.nextQuestion();
      }, 20000);
    }
  }

  private loadResult(event: RoomEvent): void {
    if(event.result){
      console.log("end of game ");
      this.result = event.result;
    }
  }

  connect(): void {
    if (this.room) {
      this.roomService.ConnectRoom(this.room.id).then(e => {
        console.log("connected!!!", e)
      });
    }
  }
  disconnect(): void {
    if (this.room) {
      this.roomService.DisconnectRoom(this.room.id).then(e => {

        this.router.navigate(['/rooms/']);
      });
    }
  }

  start(): void {
    if (this.room && this.room.players.length >= 1 && !this.starting) {
      this.starting = true;
      this.roomService.StartRoom(this.room.id).then(e => {
        this.started = true;
        this.master = true;
        this.starting = false;
        console.log("start!!!", e)
      });
    }
  }

  nextQuestion(): void {
    if (this.master) {
      if (this.room) {
        this.resetCurrent();
        this.roomService.NextQuestionForRoom(this.room.id).then(e => {
         
        });
      }
    }
  }

  private resetCurrent(): void{
    this.currentFound = false;
    this.currentShortcut = [];
    this.responseSending = false;
  }

  send(response: string[]): void {
    if (this.room && this.question && !this.responseSending && !this.currentFound) {
      this.responseSending = true;
      this.currentShortcut = response;
      this.roomService.TryResponse(this.room.id, this.question.id, response).then(e => {
        this.responseSending = false;
        console.log("trye !!", e)
        if(e){
          this.currentFound = true;
        }else{
          this.currentShortcut = [];
        }
      });
    }
  }
  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.disconnect()
  }

  @HostListener('window:keydown',['$event'])
  onKeyPress($event: KeyboardEvent) {
    console.log($event);
    $event.preventDefault();
    $event.stopPropagation();
    $event.stopImmediatePropagation();

    if($event.key != null && !QuizComponent.ignoreKey.includes($event.key)){
      var keys = [];
      if($event.ctrlKey){
        keys.push('CTRL');
      }
      if($event.altKey){
        keys.push('ALT');
      }
      if($event.shiftKey){
        keys.push('SHIFT');
      }
      keys.push($event.key.toLowerCase());
      this.send(keys);
    }
  }
}