import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Hub } from '@aws-amplify/core';
import { AuthenticatorService } from '@aws-amplify/ui-angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  constructor(private router: Router, public authenticator: AuthenticatorService) { }

  ngOnInit(): void {
    this.authenticator.subscribe(() => {
      const { route } = this.authenticator;
      
      if (route === 'authenticated') {
        this.router.navigate(['/rooms'])
      }
    });
  }

}
