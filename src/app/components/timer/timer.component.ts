import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent implements OnInit {
  @Input()
  public start : Date;
  @Input()
  public deadline : Date;
  @Input()
  public found : boolean = false;

  


  public test : String = "283 283";

  public second : number = 0;
  private max: number = 0;

  constructor() {
    this.start = new Date();
    this.deadline = new Date();
   }

  ngOnInit(): void {
    this.initMax();
    this.update();
    
    setInterval(()=>{
      this.update();
    }, 1000);
  }

  private initMax(): void{
    var diff = this.start.getTime() - this.deadline.getTime();

    var diffInSecond = Math.round(diff / 1000);
    this.max = Math.abs(diffInSecond)-1;
  }

  private update(): void{
    const currendDate = new Date();
    var diff = currendDate.getTime() - this.deadline.getTime();

    var diffInSecond = Math.round(diff / 1000);
    this.second = Math.abs(diffInSecond);

    var t = 283;
    if(this.max != 0){
      t = ((this.second-1) / this.max) * 283
    }else{
      t=283
    }
    this.test = t+" 283";
  }
}
