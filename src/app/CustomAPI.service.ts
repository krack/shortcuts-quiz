/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.
import { Injectable } from "@angular/core";
import API, { graphqlOperation, GraphQLResult } from "@aws-amplify/api-graphql";
import { Observable } from "zen-observable-ts";
import { SubscriptionResponse, __SubscriptionContainer } from "./API.service";

@Injectable({
  providedIn: "root"
})
export class CustomAPIService {
  SubscribeToEventChallengeListener(): Observable<
    SubscriptionResponse<
      Pick<__SubscriptionContainer, "subscribeToEventRoom">
    >
  > {
    const statement = `subscription SubscribeToEventRoom {
        subscribeToEventRoom {
          __typename
          id
          roomId
          type
          player {
            __typename
            id
            name
            profilePicture{
              url
            }
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    return API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    ) as Observable<
      SubscriptionResponse<
        Pick<__SubscriptionContainer, "subscribeToEventRoom">
      >
    >;
  }
}
